#!/usr/bin/bash
#Общие заголовки. Без наличия как минимум User-Agent API ведет себя неустойчиво
declare -a COMMON_HEADERS=("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"\
	"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"\
	"Accept-Language: en-US,en;q=0.5"\
	"Accept-Encoding: gzip, deflate"\
	"upgrade-insecure-requests:1"
	)

RESPONSE=$(http --follow --print HBhb https://maps.yandex.ru "${COMMON_HEADERS[@]}" )
echo "$RESPONSE" > request_1.txt

#Вытаскиваем значение csrfToken
TOKEN_CODE=$(echo "$RESPONSE" | grep -o '"csrfToken":"[^"]*"')
CSRF_TOKEN=$(echo -n "$TOKEN_CODE" | sed -n -e 's/"csrfToken":"\([^"]*\)"/\1/p')
echo $CSRF_TOKEN
#TOKEN_CODE='"csrfToken":"768aac23d1e22818ab2010a1fad9f6455347d5d5:1462954628591"'

#Значение сессионной куки
COOKIE_CODE=$(echo "$RESPONSE" | grep -o "^Set-Cookie: yandexuid.*")
COOKIE=$(echo -n "$COOKIE_CODE" | sed -n -e 's/Set-Cookie: yandexuid=\([^;]*\);.*/\1/p')
echo $COOKIE

#Запрос информации по адресу
QUERY="Ижевск, ул. Удмуртская, 11"
URL="https://yandex.ru/maps/api/search?text=${QUERY}&lang=ru_RU&csrfToken=${CSRF_TOKEN}"
echo $URL
RESPONSE=$(http  --follow --print HBhb GET "$URL" "${COMMON_HEADERS[@]}" "Cookie:yandexuid=$COOKIE")
echo "$RESPONSE" > request_2.txt
echo "$RESPONSE" | grep -o '{"data".*' | json_pp > request_2_pretty.json

#Первая из строк вида
#"InternalToponymInfo":{"geoid":44,"houses":0,"Point":{"type":"Point","coordinates":[53.234272,56.824656]}}
TOPONYM_INFO_CODE=$(echo $RESPONSE | grep --color -P -o  '"InternalToponymInfo":{[^{}]*"Point":{"type":"Point","coordinates":[^{}]*}[^{}]*}' | head -n 1 )

echo $TOPONYM_INFO_CODE | grep -P -o "\[.*\]"



