#!/usr/bin/bash
declare -a COMMON_HEADERS=("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"\
	"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"\
	"Accept-Language: en-US,en;q=0.5"\
	"Accept-Encoding: gzip, deflate"\
	"upgrade-insecure-requests:1"
	)

RESPONSE=$(http --session='.\\session.json' --follow --print HBhb https://maps.yandex.ru "${COMMON_HEADERS[@]}" )
echo "$RESPONSE" > request_1.txt

TOKEN_CODE=$(echo "$RESPONSE" | grep -o '"csrfToken":"[^"]*"')
#TOKEN_CODE='"csrfToken":"768aac23d1e22818ab2010a1fad9f6455347d5d5:1462954628591"'

CSRF_TOKEN=$(echo -n "$TOKEN_CODE" | sed -n -e 's/"csrfToken":"\([^"]*\)"/\1/p')
echo $CSRF_TOKEN

QUERY="Ижевск, Бородина, 19"
URL="https://yandex.ru/maps/api/search?text=${QUERY}&lang=ru_RU&csrfToken=${CSRF_TOKEN}"
echo $URL
RESPONSE=$(http --session='.\\session.json' --follow --print HBhb GET "$URL" "${COMMON_HEADERS[@]}")
echo "$RESPONSE" > request_2.txt


echo "$RESPONSE" | grep -o '{"data".*' | json_pp > request_2_pretty.json



