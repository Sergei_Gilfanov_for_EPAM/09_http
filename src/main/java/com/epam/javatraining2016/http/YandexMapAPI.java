package com.epam.javatraining2016.http;

import java.net.URL;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class YandexMapAPI {
  private static final Logger log = LoggerFactory.getLogger(YandexMapAPI.class);
  private static final String CharsetName = "utf-8";
  private static URI sessionURI;
  private static URI apiURI;

  String token;
  CookieManager cookies;

  private void setCommonProperties(URLConnection connection) {
    connection.setRequestProperty("User-Agent",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0");
    connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    connection.setRequestProperty("upgrade-insecure-requests", "1");
  }

  private void openSession() throws IOException, URISyntaxException {
    URL sessionURL = sessionURI.toURL();
    HttpURLConnection connection = (HttpURLConnection) sessionURL.openConnection();
    setCommonProperties(connection);
    connection.setRequestProperty("Accept", "text/html");
    connection.setRequestProperty("Accept-Charset", CharsetName);
    connection.connect();
    String contentType = connection.getContentType();
    if (!contentType.equals("text/html; charset=" + CharsetName)) {
      log.error("YandexMapAPI initialisation: Yandex site don't respect Accept* headers");
      throw new IOException();
    }

    token = findCsrfToken(connection);
    cookies = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
    cookies.put(sessionURI, connection.getHeaderFields());
  }

  private String findCsrfToken(HttpURLConnection connection) throws IOException {
    String retval = null;
    InputStream responseStream = connection.getInputStream();
    try (BufferedReader responseReader =
        new BufferedReader(new InputStreamReader(responseStream, CharsetName))) {
      Pattern tokenPattern = Pattern.compile("\"csrfToken\":\"([^\"]*)\"");
      // Результат сопоставления с пустой строкой использоваться не будет - операция нужна
      // для создания повторно используемого Matcher
      Matcher matcher = tokenPattern.matcher("");
      retval = null;
      String line = null;
      while ((line = responseReader.readLine()) != null) {
        matcher.reset(line);
        if (matcher.find()) {
          retval = matcher.group(1);
          break;
        }
      }
      if (retval == null) {
        log.error("YandexMapAPI initialisation: can't find csrfToken in Yandex response");
        throw new IOException();
      }
    }
    return retval;
  }

  private URL buildQueryURL(String query)
      throws URISyntaxException, MalformedURLException, UnsupportedEncodingException {
    // FIXME функция выдает неправильный результат, если query содержит знак '=' или '&'
    // к тому же оно идеологически не правильно. URLEncoder предназначен для кодирования
    // данных для метода POST, а не GET. Например, он заменяет пробелы на '+' а не на %20
    String urlString = String.format("%s?text=%s&lang=ru_RU&csrfToken=%s", apiURI.toString(),
        URLEncoder.encode(query, "utf-8"), token);
    URL retval = new URL(urlString);
    return retval;
  }

  public YandexMapAPI() throws IOException, URISyntaxException {
    if (sessionURI == null) {
      sessionURI = new URI("https://maps.yandex.ru");
      apiURI = new URI("https://yandex.ru/maps/api/search");
    }
    openSession();
  }

  private JsonNode getJSONFromUrl(URL url) throws IOException {
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    setCommonProperties(connection);
    connection.setRequestProperty("Accept", "application/json");
    connection.setRequestProperty("Accept-Charset", CharsetName);
    String cookieHeaderValue = cookies.getCookieStore().get(apiURI).stream()
        .map((cookie) -> cookie.toString()).collect(Collectors.joining(";"));
    connection.setRequestProperty("Cookie", cookieHeaderValue);
    connection.connect();
    String contentType = connection.getContentType();

    if (!contentType.equals("application/json; charset=" + CharsetName)) {
      log.error("getJSONFromUrl: Yandex site don't respect Accept* headers");
      throw new IOException();
    }

    InputStream responseStream = connection.getInputStream();
    JsonNode retval;
    try (BufferedReader responseReader =
        new BufferedReader(new InputStreamReader(responseStream, CharsetName))) {
      ObjectMapper mapper = new ObjectMapper();
      retval = mapper.readTree(responseReader);
    }
    return retval;
  }

  public String getCoordinates(String query) throws URISyntaxException, IOException {
    URL queryURL = buildQueryURL(query);
    log.debug("{}", queryURL);
    JsonNode apiAnswer = getJSONFromUrl(queryURL);

    String coordinates = findCoordinates(apiAnswer);
    return coordinates;
  }

  private String findCoordinates(JsonNode apiAnswer) {
    List<JsonNode> toponymContainers = apiAnswer.findParents("InternalToponymInfo");
    for (JsonNode node : toponymContainers) {
      log.debug("{}", node.get("text"));
      log.debug("{}", node.at("/InternalToponymInfo/Point/coordinates"));
    }
    String retval = null;
    if (toponymContainers.size() == 0) {
      retval = "";
    } else {
      retval = toponymContainers.get(0).at("/InternalToponymInfo/Point/coordinates").toString();
    }
    return retval;
  }
}
