package com.epam.javatraining2016.http;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {

  public static void main(String[] args) throws IOException, URISyntaxException {
    YandexMapAPI api = new YandexMapAPI();
    System.out.println(api.getCoordinates("Россия, гор. Ижевск, ул. Удмуртская, 11"));
  }

}
